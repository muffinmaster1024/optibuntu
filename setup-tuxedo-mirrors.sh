sudo sed -i 's/^/#/g' /etc/apt/sources.list

wget -O - https://deb.tuxedocomputers.com/0x54840598.pub.asc | gpg --dearmor > 0x54840598.pub.gpg
cat 0x54840598.pub.gpg | sudo tee -a /usr/share/keyrings/tuxedo-keyring.gpg > /dev/null
echo 'deb [arch=amd64 signed-by=/usr/share/keyrings/tuxedo-keyring.gpg] https://deb.tuxedocomputers.com/ubuntu jammy main' | sudo tee -a /etc/apt/sources.list.d/tuxedo-computers.list
