#!/bin/bash

./no-snap.sh

#PPAs from ppa.lst
for ppa in $(cat ppa.lst)
do
    sudo add-apt-repository ppa:$ppa -y
done

sudo apt install -y apt-transport-https curl wget

./setup-tuxedo-mirrors.sh
./setup-element.sh -m
./setup-signal.sh -m
./setup-brave-browser.sh -m
./setup-nala.sh -m
./setup-codium.sh -m

cat bash_aliases >> $HOME/.bash_aliases
source $HOME/.bash_aliases
sudo cp $HOME/.bash_aliases /root/.bash_aliases

head -1 ps-prompts >> $HOME/.bashrc
sudo tail -1 ps-prompts >> /root/.bashrc

sudo apt update
#sudo apt upgrade -y

#APT from apt.lst
for apt in $(cat apt.lst)
do
    sudo apt install $apt -y
done

for apt in $(cat apt-rmv.lst)
do
    sudo apt remove $apt -y
done
sudo apt autoremove

flatpak remote-add --if-not-exists flathub https://flathub.org/repo/flathub.flatpakrepo

#FLATPAKs from flatpak .lst
for flat in $(cat flatpak.lst)
do
    flatpak install $flat
done

#./firefox-from-source.sh #now installed through tuxedo-mirror

./sub.sh
