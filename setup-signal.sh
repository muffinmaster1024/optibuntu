#!/bin/bash
#These instructions come from https://signal.org/en/download/#

# NOTE: These instructions only work for 64 bit Debian-based
# Linux distributions such as Ubuntu, Mint etc.

# 1. Install our official public software signing key
wget -O- https://updates.signal.org/desktop/apt/keys.asc | gpg --dearmor > signal-desktop-keyring-temp.gpg
cat signal-desktop-keyring-temp.gpg | sudo tee -a /usr/share/keyrings/signal-desktop-keyring.gpg > /dev/null
rm signal-desktop-keyring-temp.gpg

# 2. Add our repository to your list of repositories
echo 'deb [arch=amd64 signed-by=/usr/share/keyrings/signal-desktop-keyring.gpg] https://updates.signal.org/desktop/apt xenial main' |\
  sudo tee -a /etc/apt/sources.list.d/signal-xenial.list

# 3. Update your package database and install signal

if ! [[ "$1" == "-m" ]]; then
    sudo apt update && sudo apt install signal-desktop -y
fi

# Use local Desktop file and configure it to start with tray-icon as default
mkdir -p ~/.local/share/applications/
cp /usr/share/applications/signal-desktop.desktop ~/.local/share/applications/signal-desktop.desktop
sed -i 's/Exec=signal-desktop /Exec=signal-desktop --use-tray-icon /' ~/.local/share/applications/signal-desktop.desktop.signal-desktop.desktop
